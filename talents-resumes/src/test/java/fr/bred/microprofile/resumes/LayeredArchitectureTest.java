package fr.bred.microprofile.resumes;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.library.Architectures.layeredArchitecture;

@AnalyzeClasses(packages = "fr.bred.microprofile.resumes")
public class LayeredArchitectureTest {
    @ArchTest
    static final ArchRule layerDependenciesAreRespected = layeredArchitecture()
            .layer("Application").definedBy("fr.bred.microprofile.resumes.application..")
            .layer("Domain").definedBy("fr.bred.microprofile.resumes.domain..")
            .layer("Infrastructure").definedBy("fr.bred.microprofile.resumes.infrastructure..")
            .layer("Interfaces").definedBy("fr.bred.microprofile.resumes.interfaces..")
            .whereLayer("Interfaces").mayNotBeAccessedByAnyLayer()
            .whereLayer("Application").mayOnlyBeAccessedByLayers("Interfaces", "Infrastructure");
}
