package fr.bred.microprofile.resumes;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.library.Architectures.onionArchitecture;

@AnalyzeClasses(packages = "fr.bred.microprofile.resumes")
public class HexagonalArchitectureTest {
    @ArchTest
    static final ArchRule hexagonalArchitectureIsRespected = onionArchitecture()
            .domainModels("..domain..")
            .domainServices("..domain..")
            .applicationServices("..application..")
            .adapter("bus", "..infrastructure.bus..")
            .adapter("persistence", "..infrastructure.persistence..")
            .adapter("rest", "..interfaces.rest..");
}
