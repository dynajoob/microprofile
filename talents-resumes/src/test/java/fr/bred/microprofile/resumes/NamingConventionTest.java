package fr.bred.microprofile.resumes;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import javax.ws.rs.Path;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

@AnalyzeClasses(packages = "fr.bred.microprofile.resumes")
public class NamingConventionTest {
    @ArchTest
    static ArchRule resourcesShouldBeSuffixed = classes()
            .that().resideInAPackage("..interfaces.rest..")
            .and().areAnnotatedWith(Path.class)
            .should().haveSimpleNameEndingWith("Resource")
            .as("Resources should be suffixed");

    @ArchTest
    static final ArchRule interfacesShouldNotHaveNamesEndingWithTheWordInterface = noClasses()
            .that().areInterfaces()
            .should().haveNameMatching(".*Interface");

}
