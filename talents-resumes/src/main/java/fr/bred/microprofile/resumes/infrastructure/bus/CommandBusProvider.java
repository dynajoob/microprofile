package fr.bred.microprofile.resumes.infrastructure.bus;

import fr.bred.microprofile.commons.application.command.CommandBus;
import fr.bred.microprofile.resumes.application.command.ResumeCommandHandler;
import fr.bred.microprofile.resumes.domain.ResumeRepository;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.Serializable;

public class CommandBusProvider implements Serializable {

    @Inject
    private ResumeRepository resumeRepository;

    @Produces
    public CommandBus getCommandBus() {
        CommandBus commandBus = GuavaCommandBus.syncGuavaCommandBus();
        commandBus.register(new ResumeCommandHandler(resumeRepository));
        return commandBus;
    }
}
