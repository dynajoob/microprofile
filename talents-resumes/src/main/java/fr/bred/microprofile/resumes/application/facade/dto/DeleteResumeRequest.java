package fr.bred.microprofile.resumes.application.facade.dto;

import fr.bred.microprofile.commons.interfaces.TransportObject;
import lombok.Data;

@Data
public class DeleteResumeRequest extends TransportObject {
    Long id;
}
