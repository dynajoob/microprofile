package fr.bred.microprofile.resumes.infrastructure.events;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import fr.bred.microprofile.commons.domain.AggregateRoot;
import fr.bred.microprofile.commons.domain.GenericId;
import fr.bred.microprofile.commons.domain.event.DomainEvent;
import fr.bred.microprofile.commons.domain.event.DomainEventStore;
import org.bson.Document;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;

import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@EventStore(EventStore.StoreType.MONGO_DB)
public class MongoDBEventStore implements DomainEventStore {

    private static Logger logger = Logger.getLogger(GuavaDomainEventBus.class.getName());

    @Inject
    MongoDatabase database;

    @Override
    public List<DomainEvent> loadEvents(GenericId id) {
        return null;
    }

    @Override
    public void save(GenericId id, Class<? extends AggregateRoot> type, List<DomainEvent> events) {

    }

    public void save(AggregateRoot aggregateRoot) {
        logger.log(Level.INFO, "Saving aggregate events to database");
        if (aggregateRoot.hasUncommittedEvents()) {
            List<DomainEvent> events = aggregateRoot.getUncommittedEvents();
            MongoCollection<Document> eventsCollection = database.getCollection("events");
            for (DomainEvent event : events) {
                Document eventDocument = new Document();
                eventDocument.put("aggregateId", aggregateRoot.id());
                eventDocument.put("aggregateClass", aggregateRoot.getClass().toString());
                eventDocument.put("event", event.getClass().toString());
                eventDocument.put("payload", event);
                eventsCollection.insertOne(eventDocument);
            }
        }
        publish(aggregateRoot);
    }

    @Override
    public List<DomainEvent> getAllEvents() {
        return null;
    }

    private DomainEvent toDomainEvent(String event) {
        Jsonb jsonb = JsonbBuilder.create();
        return jsonb.fromJson(event, DomainEvent.class);
    }

    private String toString(DomainEvent domainEvent) {
        Jsonb jsonb = JsonbBuilder.create();
        return jsonb.toJson(domainEvent);
    }

    @Incoming("consumers")
    @Outgoing("producers")
    public String publish(AggregateRoot aggregateRoot) {
        logger.log(Level.INFO, "Handling kafka events");
        return aggregateRoot.toString();
    }
}
