package fr.bred.microprofile.resumes.infrastructure.events;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import fr.bred.microprofile.commons.domain.event.DomainEvent;
import fr.bred.microprofile.commons.domain.event.DomainEventBus;
import fr.bred.microprofile.commons.domain.event.DomainEventListener;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GuavaDomainEventBus implements DomainEventBus {

    private static Logger logger = Logger.getLogger(GuavaDomainEventBus.class.getName());

    private final EventBus eventBus;
    private final EventBus replayBus;

    public GuavaDomainEventBus() {
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        eventBus = new AsyncEventBus("eventBus", executorService);
        replayBus = new AsyncEventBus("replayBus", executorService);
    }

    @Override
    public void publish(List<DomainEvent> events) {
        for (DomainEvent event : events) {
            eventBus.post(event);
        }
    }

    @Override
    public void replay(List<DomainEvent> events) {
        logger.log(Level.INFO, "Replaying [{}] events", events.size());
        for (DomainEvent event : events) {
            replayBus.post(event);
        }
    }

    @Override
    public <T extends DomainEventListener> T register(T listener) {
        if (listener.supportsReplay()) {
            replayBus.register(listener);
        }
        eventBus.register(listener);
        return listener;
    }
}
