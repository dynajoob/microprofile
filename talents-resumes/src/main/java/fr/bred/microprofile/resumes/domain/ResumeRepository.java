package fr.bred.microprofile.resumes.domain;

import fr.bred.microprofile.commons.domain.Repository;
import fr.bred.microprofile.commons.domain.vo.Email;

import java.util.List;

public interface ResumeRepository extends Repository<Resume, ResumeId> {
    List<Resume> findByEmail(Email email);
}
