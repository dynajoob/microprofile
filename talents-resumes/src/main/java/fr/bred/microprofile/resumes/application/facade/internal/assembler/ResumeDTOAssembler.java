package fr.bred.microprofile.resumes.application.facade.internal.assembler;

import fr.bred.microprofile.commons.domain.vo.Email;
import fr.bred.microprofile.resumes.domain.Resume;
import fr.bred.microprofile.resumes.application.facade.dto.ResumeDTO;

public class ResumeDTOAssembler {

    public Resume toResume(ResumeDTO resumeDTO) {
        Resume resume = new Resume();
        resume.setTitle(resumeDTO.getTitle());
        resume.setEmail(new Email(resumeDTO.getEmail()));
        return resume;
    }
}
