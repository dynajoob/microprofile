package fr.bred.microprofile.resumes.interfaces.rest;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import org.eclipse.microprofile.health.Readiness;

import javax.enterprise.context.ApplicationScoped;

@Readiness
@ApplicationScoped
public class ApplicationReadinessCheck implements HealthCheck {
    @Override
    public HealthCheckResponse call() {
        HealthCheckResponseBuilder responseBuilder = HealthCheckResponse.named("api");
        String serverName = System.getProperty("wlp.server.name");
        if (!serverName.equals("defaultServer")) {
            return responseBuilder.withData(serverName, "not available").down()
                    .build();
        }
        return responseBuilder.withData(serverName, "available").up().build();
    }
}
