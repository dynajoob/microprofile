package fr.bred.microprofile.resumes.application.command;

import com.google.common.eventbus.Subscribe;
import fr.bred.microprofile.commons.application.command.CommandHandler;
import fr.bred.microprofile.commons.domain.vo.Email;
import fr.bred.microprofile.resumes.domain.LimitExceededException;
import fr.bred.microprofile.resumes.domain.Resume;
import fr.bred.microprofile.resumes.domain.ResumeRepository;

public class ResumeCommandHandler implements CommandHandler {

    private ResumeRepository resumeRepository;

    public ResumeCommandHandler(ResumeRepository resumeRepository) {
        this.resumeRepository = resumeRepository;
    }

    @Subscribe
    public void handle(CreateResumeCommand createResumeCommand) throws LimitExceededException {
        Email email = new Email(createResumeCommand.getEmail());
        if(resumeRepository.findByEmail(email).size() >= 1) {
            throw new LimitExceededException();
        }
        Resume resume = new Resume();
        resume.setTitle(createResumeCommand.getTitle());
        resume.setEmail(email);
        resume.activate();
        resumeRepository.create(resume);
    }

    @Subscribe
    public void handle(DeleteResumeCommand deleteResumeCommand) {

    }
}
