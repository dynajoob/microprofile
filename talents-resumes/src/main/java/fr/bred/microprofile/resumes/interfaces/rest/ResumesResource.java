package fr.bred.microprofile.resumes.interfaces.rest;

import fr.bred.microprofile.commons.application.command.CommandBus;
import fr.bred.microprofile.resumes.application.command.CreateResumeCommand;
import fr.bred.microprofile.resumes.application.command.DeleteResumeCommand;
import fr.bred.microprofile.resumes.application.command.ResumeCommandFactory;
import fr.bred.microprofile.resumes.domain.Resume;
import fr.bred.microprofile.resumes.domain.ResumeId;
import fr.bred.microprofile.resumes.domain.ResumeRepository;
import fr.bred.microprofile.resumes.application.facade.dto.CreateResumeRequest;
import fr.bred.microprofile.resumes.application.facade.dto.DeleteResumeRequest;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.eclipse.microprofile.openapi.annotations.Operation;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Path("/resumes")
@Produces(MediaType.APPLICATION_JSON)
public class ResumesResource {

    private static Logger logger = Logger.getLogger(ResumesResource.class.getName());
    private final ResumeCommandFactory commandFactory = new ResumeCommandFactory();

    @Inject
    private CommandBus commandBus;

    @Inject
    private ResumeRepository resumeRepository;

    private File file;

    @GET
    @Path("{id}")
    @Operation(summary = "Return user resume")
    @Transactional
    public Response readResume(@PathParam("id") ResumeId resumeId) {
        Resume resume = resumeRepository.load(resumeId);
        if (resume != null) {
            return Response.ok(resume).build();
        } else {
            logger.log(Level.SEVERE, "Resume with identifier ''{0}'' not found", resumeId);
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Operation(summary = "Return search resumes")
    public Response findResume() {
        try(Stream<Resume> stream = resumeRepository.findAll()) {
            List<Resume> surveys = stream.limit(2)
                    .collect(Collectors.toList());
            return Response.status(Response.Status.PARTIAL_CONTENT)
                    .entity(surveys)
                    .build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Create new resume")
    @Transactional
    public void createResume(@Valid CreateResumeRequest request) {
        CreateResumeCommand createResumeCommand = commandFactory.toCommand(request);
        commandBus.dispatch(createResumeCommand);
    }

    @DELETE
    @RolesAllowed({ "admin", "user" })
    @Operation(summary = "Delete user resume")
    @Transactional
    public Response deleteResume(@Valid DeleteResumeRequest request) {
        DeleteResumeCommand deleteResumeCommand = commandFactory.toCommand(request);
        commandBus.dispatch(deleteResumeCommand);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @POST
    @Path("attach")
    @Operation(summary = "Upload resume")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response attachFile(@Context HttpServletRequest request) {
        int maxFileSize = 10 * 1024 * 1024;
        int maxMemSize = 4 * 1024;
        logger.log(Level.INFO, "Uploading file");
        String filePath = "E://opt//dynajoob//uploads//";
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setDefaultCharset(String.valueOf(StandardCharsets.UTF_8));
        factory.setSizeThreshold(maxMemSize);
        factory.setRepository(new File("E://tmp"));
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setSizeMax(maxFileSize);
        try {
            List<FileItem> items = upload.parseRequest(request);
            for (FileItem fileItem : items) {
                String fileName = fileItem.getName();
                if( fileName.lastIndexOf("\\") >= 0 ) {
                    file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
                } else {
                    file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")+1));
                }
                fileItem.write(file);
            }
        } catch (Exception exception) {
            logger.log(Level.SEVERE, "Error when uploading file {}", exception.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.status(Response.Status.NO_CONTENT).build();
    }
}
