package fr.bred.microprofile.resumes.interfaces.rest;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class CORSConfig {
    @Inject
    @ConfigProperty(name = "api.origin", defaultValue = "*")
    private String origin;

    public String getOrigin() {
        return origin;
    }
}
