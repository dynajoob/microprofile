package fr.bred.microprofile.resumes.domain;

import fr.bred.microprofile.commons.domain.ValueObject;

public enum ResumeStatus implements ValueObject {
    ANONYMOUS,
    PRIVATE,
    PUBLIC;

    @Override
    public boolean sameValueAs(Object other) {
        return false;
    }
}
