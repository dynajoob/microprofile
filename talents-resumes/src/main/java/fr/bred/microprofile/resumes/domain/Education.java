package fr.bred.microprofile.resumes.domain;

import fr.bred.microprofile.commons.domain.Entity;

public class Education implements Entity<Education> {
    private Long id;
    private String area;
    private String summary;
    private String title;
}
