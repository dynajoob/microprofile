package fr.bred.microprofile.resumes.infrastructure.events;

import com.google.common.collect.Lists;
import fr.bred.microprofile.commons.domain.AggregateRoot;
import fr.bred.microprofile.commons.domain.GenericId;
import fr.bred.microprofile.commons.domain.event.DomainEvent;
import fr.bred.microprofile.commons.domain.event.DomainEventStore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@EventStore(EventStore.StoreType.IN_MEMORY)
public class InMemoryDomainEventStore implements DomainEventStore {
    private final List<DomainEvent> events = new ArrayList<>();

    @Override
    public List<DomainEvent> loadEvents(GenericId id) {
        List<DomainEvent> loadedEvents = new ArrayList<>();
        for (DomainEvent event : events) {
            if (event.aggregateId.equals(id)) {
                loadedEvents.add(event);
            }
        }
        if (loadedEvents.isEmpty()) throw new IllegalArgumentException("Aggregate does not exist: " + id);
        else return loadedEvents;
    }

    @Override
    public void save(GenericId id, Class<? extends AggregateRoot> type, List<DomainEvent> events) {
        this.events.addAll(events);
    }

    @Override
    public void save(AggregateRoot aggregateRoot) {

    }

    @Override
    public List<DomainEvent> getAllEvents() {
        List<DomainEvent> domainEvents = Lists.newArrayList(events.iterator());
        Collections.reverse(domainEvents);
        return domainEvents;
    }
}
