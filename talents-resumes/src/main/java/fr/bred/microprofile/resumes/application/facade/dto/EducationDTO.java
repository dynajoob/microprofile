package fr.bred.microprofile.resumes.application.facade.dto;

import lombok.Data;

@Data
public class EducationDTO {
    private String area;
    private String summary;
    private String title;
}
