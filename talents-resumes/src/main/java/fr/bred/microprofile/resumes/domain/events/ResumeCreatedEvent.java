package fr.bred.microprofile.resumes.domain.events;

import fr.bred.microprofile.commons.domain.event.DomainEvent;
import fr.bred.microprofile.resumes.domain.ResumeId;

public class ResumeCreatedEvent extends DomainEvent<ResumeId> {
    protected ResumeCreatedEvent(ResumeId aggregateId, int version, long timestamp) {
        super(aggregateId, version, timestamp);
    }
}
