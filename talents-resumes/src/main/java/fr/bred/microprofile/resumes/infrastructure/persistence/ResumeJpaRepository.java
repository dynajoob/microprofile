package fr.bred.microprofile.resumes.infrastructure.persistence;

import fr.bred.microprofile.commons.domain.event.DomainEvent;
import fr.bred.microprofile.commons.domain.event.DomainEventBus;
import fr.bred.microprofile.commons.domain.event.DomainEventStore;
import fr.bred.microprofile.commons.domain.vo.Email;
import fr.bred.microprofile.resumes.domain.Resume;
import fr.bred.microprofile.resumes.domain.ResumeId;
import fr.bred.microprofile.resumes.domain.ResumeRepository;
import fr.bred.microprofile.resumes.infrastructure.events.EventStore;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

public class ResumeJpaRepository implements ResumeRepository {

    @Inject
    private DomainEventBus domainEventBus;

    @Inject
    @EventStore(EventStore.StoreType.MONGO_DB)
    private DomainEventStore domainEventStore;

    @PersistenceContext(name = "dynajoob-persistence")
    private EntityManager entityManager;

    @Override
    public void create(Resume resume) {
        if (resume.hasUncommittedEvents()) {
            List<DomainEvent> events = resume.getUncommittedEvents();
            resume.setId(ResumeId.randomId());
            domainEventStore.save(resume.getId(), Resume.class, events);
            domainEventBus.publish(events);
        }
    }

    @Override
    public Stream<Resume> findAll() {
        return entityManager.createNamedQuery("Resume.findAll", Resume.class).getResultStream();
    }

    @Override
    public List<Resume> findByEmail(Email email) {
        return entityManager.createQuery("SELECT resume FROM Resume resume WHERE resume.email = :email", Resume.class)
                .setParameter("email", email)
                .getResultList();
    }

    @Override
    public Resume load(ResumeId resumeId) {
        return entityManager.find(Resume.class, resumeId);
    }

    public ResumeId nextIdentity() {
        return new ResumeId(UUID.randomUUID().toString().toUpperCase());
    }
}
