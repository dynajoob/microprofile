package fr.bred.microprofile.resumes.domain;

import fr.bred.microprofile.commons.domain.GenericId;

import java.util.UUID;

public class ResumeId extends GenericId {

    public ResumeId(String id) {
        super(id);
    }

    public static ResumeId randomId() {
        return new ResumeId(UUID.randomUUID().toString());
    }

    protected ResumeId() {
        super();
    }
}
