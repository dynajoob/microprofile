package fr.bred.microprofile.resumes.domain;

import fr.bred.microprofile.commons.domain.AggregateRoot;
import fr.bred.microprofile.commons.domain.vo.Email;
import fr.bred.microprofile.commons.domain.vo.Gender;
import fr.bred.microprofile.resumes.domain.events.ResumeActivatedEvent;
import lombok.Data;

import java.util.Date;

@Data
public class Resume extends AggregateRoot<ResumeId> {
    private ResumeId id;
    private Date createdAt = new Date();
    private Email email;
    private String firstName;
    private Gender gender;
    private String headline;
    private String language;
    private String lastName;
    private Date modifiedAt;
    private ResumeStatus status = ResumeStatus.PRIVATE;
    private Integer strength = 5;
    private String summary;
    private String title;

    public Resume() {
    }

    public void activate() {
        if (resumeIsPrivate()) {
            applyChange(new ResumeActivatedEvent(id(), nextVersion(), now()));
        }
    }

    void handleEvent(ResumeActivatedEvent event) {
        this.status = ResumeStatus.PUBLIC;
    }

    private boolean resumeIsPrivate() {
        return status == ResumeStatus.PRIVATE;
    }
}
