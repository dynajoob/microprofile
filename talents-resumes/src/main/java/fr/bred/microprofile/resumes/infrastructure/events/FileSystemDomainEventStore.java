package fr.bred.microprofile.resumes.infrastructure.events;

import fr.bred.microprofile.commons.domain.AggregateRoot;
import fr.bred.microprofile.commons.domain.GenericId;
import fr.bred.microprofile.commons.domain.event.DomainEvent;
import fr.bred.microprofile.commons.domain.event.DomainEventStore;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.nio.charset.StandardCharsets.UTF_8;

@EventStore(EventStore.StoreType.FILE)
public class FileSystemDomainEventStore implements DomainEventStore {
    private static final String DEFAULT_FILE_NAME = "/tmp/events.db";
    private final File eventStoreFile;

    public FileSystemDomainEventStore() throws IOException {
        this(DEFAULT_FILE_NAME);
    }

    public FileSystemDomainEventStore(String eventStoreFile) throws IOException {
        File file = new File(eventStoreFile);
        if (!file.exists()) {
            if (!file.createNewFile()) {
                throw new IOException("Unable to create file: " + eventStoreFile);
            }
        }
        this.eventStoreFile = file;
    }

    @Override
    public synchronized List<DomainEvent> loadEvents(GenericId id) {
        List<DomainEvent> events = new ArrayList<>();
        for (DomainEvent event : getAllEvents()) {
            if (event.aggregateId.equals(id)) {
                events.add(event);
            }
        }
        return events;
    }

    @Override
    public void save(GenericId id, Class<? extends AggregateRoot> type, List<DomainEvent> events) {
        try {
            FileWriter fileWriter = new FileWriter(eventStoreFile, UTF_8, true);
            for (DomainEvent event : events) {
                fileWriter.write(toString(event));
            }
            fileWriter.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void save(AggregateRoot aggregateRoot) {
        if (aggregateRoot.hasUncommittedEvents()) {
            List<DomainEvent> events = aggregateRoot.getUncommittedEvents();
            save(aggregateRoot.id(), aggregateRoot.getClass(), events);
        }
    }

    @Override
    public synchronized List<DomainEvent> getAllEvents() {
        List<DomainEvent> events = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(eventStoreFile);
            while (scanner.hasNextLine()) {
                events.add(toDomainEvent(scanner.nextLine()));
            }
            scanner.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return events;
    }

    private DomainEvent toDomainEvent(String event) {
        Jsonb jsonb = JsonbBuilder.create();
        return jsonb.fromJson(event, DomainEvent.class);
    }

    private String toString(DomainEvent domainEvent) {
        Jsonb jsonb = JsonbBuilder.create();
        return jsonb.toJson(domainEvent);
    }
}
