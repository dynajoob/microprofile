package fr.bred.microprofile.resumes.interfaces.rest;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import org.eclipse.microprofile.health.Liveness;

import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.net.Socket;

@Liveness
@ApplicationScoped
public class ApplicationLivenessCheck implements HealthCheck {
    @Override
    public HealthCheckResponse call() {
        HealthCheckResponseBuilder responseBuilder = HealthCheckResponse.named("database");
        String host = (System.getenv("POSTGRESQL_SERVICE_HOST") != null) ?  System.getenv("POSTGRESQL_SERVICE_HOST") : "localhost";
        int port = (System.getenv("POSTGRESQL_SERVICE_PORT") != null) ? Integer.parseInt(System.getenv("POSTGRESQL_SERVICE_PORT")) : 5432;
        try {
            pingServer(host, port);
            responseBuilder.up();
        } catch (Exception exception) {
            responseBuilder.down().withData("error", exception.getMessage());
        }
        return responseBuilder.build();
    }

    private void pingServer(String host, int port) throws IOException {
        Socket socket = new Socket(host, port);
        socket.close();
    }
}
