package fr.bred.microprofile.resumes.interfaces.rest;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.HashMap;
import java.util.Map;

@Provider
public class ValidationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {
    @Override
    public Response toResponse(ConstraintViolationException exception) {
        Map<Path, String> errors = new HashMap<>();
        for (ConstraintViolation<?> constraintViolation : exception.getConstraintViolations()) {
            errors.put(constraintViolation.getPropertyPath(), constraintViolation.getMessage());
        }
        return Response
                .status(Response.Status.PRECONDITION_FAILED)
                .entity(errors)
                .build();
    }
}
