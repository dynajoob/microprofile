package fr.bred.microprofile.resumes.application.facade.dto;

import fr.bred.microprofile.commons.interfaces.TransportObject;
import lombok.Data;

@Data
public class CreateResumeRequest extends TransportObject {
    private String email;
    private String title;
}
