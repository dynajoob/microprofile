package fr.bred.microprofile.resumes.infrastructure.kafka;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;

import java.util.concurrent.CompletionStage;

public class Consumer {
    @Incoming("consumers")
    public CompletionStage<Void> consume(Message<String> message) {
        return message.ack();
    }
}
