package fr.bred.microprofile.resumes.infrastructure.bus;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import fr.bred.microprofile.commons.application.command.Command;
import fr.bred.microprofile.commons.application.command.CommandBus;
import fr.bred.microprofile.commons.application.command.CommandHandler;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GuavaCommandBus implements CommandBus {

    private final EventBus commandBus;

    private GuavaCommandBus(EventBus commandBus) {
        this.commandBus = commandBus;
    }

    public static CommandBus asyncGuavaCommandBus() {
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        return new GuavaCommandBus(new AsyncEventBus("commandBus", executorService));
    }

    public static CommandBus syncGuavaCommandBus() {
        return new GuavaCommandBus(new EventBus("commandBus"));
    }

    @Override
    public void dispatch(Command command) {
        commandBus.post(command);
    }

    @Override
    public <T extends CommandHandler> T register(T handler) {
        commandBus.register(handler);
        return handler;
    }
}
