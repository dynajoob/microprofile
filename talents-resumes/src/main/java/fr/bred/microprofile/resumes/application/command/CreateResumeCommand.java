package fr.bred.microprofile.resumes.application.command;

import fr.bred.microprofile.commons.application.command.Command;
import lombok.Data;

@Data
public class CreateResumeCommand implements Command {
    private String email;
    private String title;
}
