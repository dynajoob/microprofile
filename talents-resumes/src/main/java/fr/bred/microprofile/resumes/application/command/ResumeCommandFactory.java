package fr.bred.microprofile.resumes.application.command;

import fr.bred.microprofile.commons.application.command.CommandFactory;
import fr.bred.microprofile.resumes.application.facade.dto.CreateResumeRequest;
import fr.bred.microprofile.resumes.application.facade.dto.DeleteResumeRequest;

public class ResumeCommandFactory implements CommandFactory {

    public CreateResumeCommand toCommand(CreateResumeRequest request) {
        CreateResumeCommand command = new CreateResumeCommand();
        command.setEmail(request.getEmail());
        command.setTitle(request.getTitle());
        return command;
    }

    public DeleteResumeCommand toCommand(DeleteResumeRequest request) {
        DeleteResumeCommand command = new DeleteResumeCommand();
        return command;
    }
}
