package fr.bred.microprofile.resumes.application.facade.dto;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ResumeDTO {
    @Email
    @NotNull
    private String email;
    @NotNull
    private String title;
    @Valid
    private List<EducationDTO> educations;
}
