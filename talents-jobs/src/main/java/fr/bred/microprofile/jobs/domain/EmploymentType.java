package fr.bred.microprofile.jobs.domain;

import fr.bred.microprofile.commons.domain.ValueObject;

public enum EmploymentType implements ValueObject {
    FULL_TIME,
    PART_TIME,
    CONTRACTOR,
    TEMPORARY,
    INTERN,
    VOLUNTEER,
    PER_DIEM,
    OTHER;

    @Override
    public boolean sameValueAs(Object other) {
        return false;
    }
}
