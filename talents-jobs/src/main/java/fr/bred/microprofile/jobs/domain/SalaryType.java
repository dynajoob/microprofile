package fr.bred.microprofile.jobs.domain;

import fr.bred.microprofile.commons.domain.ValueObject;

public enum SalaryType implements ValueObject {
    PER_HOUR,
    PER_MONTH,
    PER_YEAR,
    PER_WEEK;

    @Override
    public boolean sameValueAs(Object other) {
        return false;
    }
}
