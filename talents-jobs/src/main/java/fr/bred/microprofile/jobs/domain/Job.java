package fr.bred.microprofile.jobs.domain;

import fr.bred.microprofile.commons.domain.AggregateRoot;
import lombok.Data;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.Date;

@Data
@Schema(name="Job", description="POJO that represents a single job entry.")
public class Job extends AggregateRoot {
    private MonetaryAmount baseSalary;
    private Date datePosted;
    private String description;
    private String educationRequirements;
    private EmploymentType employmentType;
    private String experienceRequirements;
    private Organization hiringOrganization;
    private Industry industry;
    private Boolean immediateStart;
    private Location location;
    private String qualifications;
    private String referenceNumber;
    private String responsibilities;
    private Date startDate;
    private String title;
    private Integer totalJobOpenings;
    private Date validThrough;
    private String workingHours;
}
