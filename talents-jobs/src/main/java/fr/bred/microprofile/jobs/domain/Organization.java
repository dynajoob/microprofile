package fr.bred.microprofile.jobs.domain;

import lombok.Data;

@Data
public class Organization {
    private Long id;
    private Job job;
    private String legalName;
    private String logo;
    private String name;
    private String slogan;
}
