package fr.bred.microprofile.jobs.domain;

import lombok.Data;

import java.util.Collection;

@Data
public class Industry {
    private Long id;
    private Collection<Job> jobs;
}
