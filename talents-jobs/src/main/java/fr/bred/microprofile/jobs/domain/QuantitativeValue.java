package fr.bred.microprofile.jobs.domain;

import lombok.Data;

@Data
public class QuantitativeValue<V, U> {
    private V value;
    private U unitText;
}
