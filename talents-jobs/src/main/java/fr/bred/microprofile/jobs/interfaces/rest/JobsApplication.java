package fr.bred.microprofile.jobs.interfaces.rest;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;

@ApplicationPath("api/v1")
@ApplicationScoped
public class JobsApplication extends javax.ws.rs.core.Application {
    @GET
    public String welcome() {
        return "Greetings";
    }
}