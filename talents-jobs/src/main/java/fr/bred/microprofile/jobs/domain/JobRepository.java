package fr.bred.microprofile.jobs.domain;

import java.util.stream.Stream;

public interface JobRepository {
    void create(Job job);
    Stream<Job> findAll();
    Job getById(Long id);
}
