package fr.bred.microprofile.jobs.domain;

import lombok.Data;

@Data
public class Location {
    private Long id;
    private String addressCountry;
    private String addressLocality;
    private String addressRegion;
    private Job job;
    private String postalCode;
    private String streetAddress;
}
