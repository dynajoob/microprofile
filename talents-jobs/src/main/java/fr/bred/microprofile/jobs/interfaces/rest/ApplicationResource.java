package fr.bred.microprofile.jobs.interfaces.rest;

import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/applications")
@RequestScoped
@Stateless
public class ApplicationResource {

    @GET
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getApplication(@PathParam("id") Long applicationId) {
        return Response.status(Response.Status.NO_CONTENT).build();
    }
}
