package fr.bred.microprofile.jobs.interfaces.rest;

import fr.bred.microprofile.jobs.application.facade.CreateJobRequest;
import fr.bred.microprofile.jobs.domain.Job;
import fr.bred.microprofile.jobs.domain.JobRepository;
import org.eclipse.microprofile.openapi.annotations.Operation;

import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Path("/jobs")
@RequestScoped
@Stateless
public class JobsResource {
    private static Logger logger = Logger.getLogger(JobsResource.class.getName());

    @Inject
    private JobRepository jobRepository;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findJob() {
        try(Stream<Job> stream = jobRepository.findAll()) {
            List<Job> jobs = stream.limit(2)
                    .collect(Collectors.toList());
            return Response.status(Response.Status.PARTIAL_CONTENT)
                    .entity(jobs)
                    .build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Create new resume")
    @Transactional
    public Response createResume(@Valid CreateJobRequest request) {
        return Response.status(Response.Status.CREATED).build();
    }
}
