package fr.bred.microprofile.jobs.infrastructure.persistence;

import fr.bred.microprofile.jobs.domain.Job;
import fr.bred.microprofile.jobs.domain.JobRepository;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.stream.Stream;

@RequestScoped
public class JobJpaRepository implements JobRepository {

    @PersistenceContext(name = "dynajoob-persistence")
    private EntityManager entityManager;

    @Override
    public void create(Job job) {

    }

    @Override
    public Stream<Job> findAll() {
        return entityManager.createNamedQuery("Job.findAll", Job.class).getResultStream();
    }

    @Override
    public Job getById(Long id) {
        return null;
    }
}
