package fr.bred.microprofile.jobs.domain;

import fr.bred.microprofile.commons.domain.vo.Currency;
import lombok.Data;

import java.time.temporal.ChronoUnit;

@Data
public class MonetaryAmount {
    private Long id;
    private Currency currency;
    private QuantitativeValue<Integer, ChronoUnit> value;
    private Job job;
}
