package fr.bred.microprofile.commons.domain.specification;

public interface Specification<T> {
    boolean isSatisfiedBy(T object);
    Specification<T> and(Specification<T> specification);
    Specification<T> or(Specification<T> specification);
    Specification<T> not(Specification<T> specification);
}
