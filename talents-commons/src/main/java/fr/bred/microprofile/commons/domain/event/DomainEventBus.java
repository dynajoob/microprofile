package fr.bred.microprofile.commons.domain.event;

import java.util.List;

public interface DomainEventBus {
    void publish(List<DomainEvent> events);
    void replay(List<DomainEvent> events);
    <T extends DomainEventListener> T register(T listener);
}
