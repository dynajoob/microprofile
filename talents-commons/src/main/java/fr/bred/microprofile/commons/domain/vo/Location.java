package fr.bred.microprofile.commons.domain.vo;

import fr.bred.microprofile.commons.domain.Entity;

public class Location implements Entity {
    private Long id;
    private String address;
    private String city;
    private String countryCode;
    private String latitude;
    private String longitude;
    private String region;
    private String state;
}
