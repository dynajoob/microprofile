package fr.bred.microprofile.commons.domain.vo;

import fr.bred.microprofile.commons.domain.ValueObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Email implements ValueObject {

    private String value;

    public Email(String value) {
        this.value = value;
    }

    @Override
    public boolean sameValueAs(Object other) {
        return false;
    }
}
