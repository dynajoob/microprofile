package fr.bred.microprofile.commons.domain.event;

import fr.bred.microprofile.commons.domain.AggregateRoot;
import fr.bred.microprofile.commons.domain.GenericId;

import java.util.List;

public interface DomainEventStore {
    List<DomainEvent> loadEvents(GenericId id);
    void save(GenericId id, Class<? extends AggregateRoot> type, List<DomainEvent> events);
    void save(AggregateRoot aggregateRoot);
    List<DomainEvent> getAllEvents();
}
