package fr.bred.microprofile.commons.domain;

public enum SurveyStatus implements ValueObject<SurveyStatus> {
    ARCHIVED, PUBLISHED;

    @Override
    public boolean sameValueAs(final SurveyStatus other) {
        return this.equals(other);
    }
}
