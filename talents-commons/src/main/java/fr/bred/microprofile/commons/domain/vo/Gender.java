package fr.bred.microprofile.commons.domain.vo;

import fr.bred.microprofile.commons.domain.ValueObject;

public enum Gender implements ValueObject {
    FEMALE,
    MALE;

    @Override
    public boolean sameValueAs(Object other) {
        return false;
    }
}
