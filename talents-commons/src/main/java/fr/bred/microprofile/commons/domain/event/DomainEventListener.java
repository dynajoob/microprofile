package fr.bred.microprofile.commons.domain.event;

public interface DomainEventListener {
    boolean supportsReplay();
}
