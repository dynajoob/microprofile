package fr.bred.microprofile.commons.domain;

import java.io.Serializable;

public class GenericId implements Serializable {

    public String id;

    public GenericId(String id) {
        this.id = id;
    }

    protected GenericId() {
    }
}
