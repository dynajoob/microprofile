package fr.bred.microprofile.commons.domain.specification;

public class OrSpecification<T> extends AbstractSpecification<T> {
    private Specification<T> specificationA;
    private Specification<T> specificationB;

    public OrSpecification(final Specification<T> specificationA, final Specification<T> specificationB) {
        this.specificationA = specificationA;
        this.specificationB = specificationB;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isSatisfiedBy(final T object) {
        return specificationA.isSatisfiedBy(object) || specificationB.isSatisfiedBy(object);
    }
}
