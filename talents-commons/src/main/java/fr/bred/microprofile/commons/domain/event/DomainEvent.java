package fr.bred.microprofile.commons.domain.event;

import fr.bred.microprofile.commons.domain.GenericId;

import java.io.Serializable;

public abstract class DomainEvent<T extends GenericId> implements Serializable {
    public final T aggregateId;
    public final int version;
    public final long timestamp;

    protected DomainEvent(T aggregateId, int version, long timestamp) {
        this.aggregateId = aggregateId;
        this.version = version;
        this.timestamp = timestamp;
    }
}
