package fr.bred.microprofile.commons.application.command;

public interface CommandBus {
    void dispatch(Command command);
    <T extends CommandHandler> T register(T handler);
}
