package fr.bred.microprofile.commons.domain;

import java.util.stream.Stream;

public interface Repository<AR extends AggregateRoot, ID extends GenericId> {
    void create(AR aggregateRoot);
    Stream<AR> findAll();
    AR load(ID id);
    ID nextIdentity();
}
