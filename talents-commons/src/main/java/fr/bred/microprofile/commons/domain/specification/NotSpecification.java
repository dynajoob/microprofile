package fr.bred.microprofile.commons.domain.specification;

public class NotSpecification<T> extends AbstractSpecification<T> {
    private Specification<T> specificationA;

    public NotSpecification(final Specification<T> specificationA) {
        this.specificationA = specificationA;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isSatisfiedBy(final T object) {
        return !specificationA.isSatisfiedBy(object);
    }
}
