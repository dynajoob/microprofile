import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { JobService } from "../../job.service";

@Component({
  selector: 'job-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  providers: [JobService]
})
export class JobCreateComponent implements OnInit {
  jobForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.jobForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.jobForm = this.formBuilder.group({
      firstName: '',
      lastName: '',
      email: '',
      drinkPreference: ''
    });
  }

  submit() {
    if (this.jobForm.valid) {
      console.log(this.jobForm.value);
    }
  }
}
