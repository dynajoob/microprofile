import { Component } from "@angular/core";
import { JobService } from "../../job.service";

@Component({
  selector: 'job-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  providers: [JobService]
})
export class JobListComponent {}
