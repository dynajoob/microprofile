import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { JobRoutingModule } from "./job-routing.module";
import { JobListComponent } from "./components/list/list.component";
import { JobCreateComponent } from "./components/create/create.component";
import { ClarityModule } from '@clr/angular';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
    JobRoutingModule
  ],
  declarations: [
    JobCreateComponent,
    JobListComponent
  ]
})
export class JobModule {}
