import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobListComponent } from 'src/app/modules/job/components/list/list.component';
import { JobCreateComponent } from 'src/app/modules/job/components/create/create.component';
import { AuthenticatedGuard } from 'src/app/core/authentication/guards/authenticated.guard';

const routes: Routes = [
  { path: 'jobs', component: JobListComponent },
  { path: 'jobs/create', component: JobCreateComponent, canActivate: [AuthenticatedGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class JobRoutingModule {}
