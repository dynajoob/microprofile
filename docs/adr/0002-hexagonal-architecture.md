# 2. Hexagonal Architecture
Date: 2019-11-10
## Status
Accepted
## Context
```plantuml
@startuml
skinparam arrowColor #0b3487
skinparam databaseBackgroundColor Grey
skinparam queueBackgroundColor Aqua
skinparam shadowing false
skinparam storageBackgroundColor Aqua
skinparam rectangle {
    backgroundColor White
    borderColor #0b3487
    roundCorner 25
}
skinparam note {
    backgroundColor #f2f2f2
    borderColor #434343
    fontColor #434343

}
title: Architecture layers
rectangle "User Interface Layer" as interface
note right of interface
    Displays some data to end users and 
    where end users interact with the system
end note
rectangle "Application Layer" as application
note right of application
    Orchestrates Domain objects to perform 
    tasks required by the end users
end note
rectangle "Domain Layer" as domain
note right of domain
    Contains all business logic, the Entities, 
    Events and any other object type that contains 
    Business Logic
end note
rectangle "Infrastructure Layer" as infrastructure
note right of infrastructure
    Technical capabilities that support the 
    layers above, e.g. persistence or messaging
end note
interface -down-> application
application -down-> domain
domain -down-> infrastructure
```
## Decision
