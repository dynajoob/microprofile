```plantuml format="svg" width="800"
@startuml
skinparam arrowColor #0b3487
skinparam databaseBackgroundColor Grey
skinparam queueBackgroundColor Aqua
skinparam shadowing false
skinparam storageBackgroundColor Aqua
skinparam actor {
    backgroundColor White
    borderColor #0b3487
}
skinparam component {
    backgroundColor White
    backgroundColor<<eureka>> #2c6eaa
    borderColor #0b3487
    borderColor<<eureka>> #2c6eaa
    fontColor<<eureka>> White
}
skinparam database {
    backgroundColor #434343
    borderColor #f2f2f2
    fontColor #f2f2f2
}
skinparam node {
    backgroundColor White
    borderColor #0b3487
}
skinparam note {
    backgroundColor #f2f2f2
    borderColor #434343
    fontColor #434343

}
skinparam queue {
    backgroundColor #434343
    borderColor #f2f2f2
    fontColor #f2f2f2
}
skinparam storage {
    backgroundColor #434343
    borderColor #f2f2f2
    fontColor #f2f2f2
}

title: Architecture

actor "User" as user
note right of user
    Guest, job seeker, recruiter or administrator
end note
node "talents-authentication" as authentication {
    component "OAuth2 Server"
    component "Eureka Client" <<eureka>> as eureka.authentication
    database "Database" <<mysql>> as database.authentication
}
note top of authentication
  OAuth2 and authentication server
end note
node "talents-config" as config
node "talents-files" as files {
    component "Eureka Client" <<eureka>> as eureka.files
    database "Database" <<mysql>> as database.files
    storage "Storage" as storage
}
note top of files
    Use cloud storage if possible
end note
node "talents-gateway" as gateway {
    component "Reverse Proxy" <<zuul>>
    component "Eureka Server" <<eureka>>
}
note right of gateway
    API Gateway, using Zuul as reverse proxy
end note
node "talents-notification" as notification {
    component "Eureka Client" <<eureka>> as eureka.notification
    database "Database" <<mysql>> as database.notification
}
node "talents-resumes" as resumes {
    component "Eureka Client" <<eureka>> as eureka.resumes
    database "Database" <<mysql>> as database.resumes
}
node "talents-parser" as resumes.parser {
    component "Eureka Client" <<eureka>> as eureka.resumes.parser
}
node "talents-ui" as ui
note left of ui
    Frontend application
    NodeJS, Angular 7 server-side rendering
end note
queue "Apache Kafka" as kafka
note bottom of kafka {
    Events sourcing
}

user --> ui : Use HTTPS for secure connections
ui .. gateway : Restful API '/api/*'
gateway ==> authentication : /api/authentication
gateway ==> files : /api/attachments
gateway ==> notification : /api/notifications
gateway ==> resumes : /api/resumes
gateway -[hidden]- resumes.parser

authentication <~~> kafka
files <~~> kafka
notification <~~> kafka
resumes <~~> kafka
resumes.parser <~~> kafka

authentication ..> config
files ..> config
notification ..> config
resumes .. config
resumes.parser ..> config
@enduml
```