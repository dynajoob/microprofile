# Kafka and JMS
```shell
kafka-topics --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic dynajoob
kafka-topics --list --bootstrap-server localhost:9092
kafka-console-producer --broker-list localhost:9092 --topic dynajoob
kafka-console-consumer --bootstrap-server localhost:9092 --topic dynajoob --from-beginning
```

## Installation